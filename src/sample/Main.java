package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import sample.database.DatabaseHandler;

import java.sql.ResultSet;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("view/login.fxml"));
        primaryStage.setTitle("TODO");
        primaryStage.getIcons().add(new Image("/sample/assets/clipart-straw-hat-5.png"));
        primaryStage.setScene(new Scene(root, 700, 400));
        primaryStage.show();

        DatabaseHandler databaseHandler = new DatabaseHandler();
        ResultSet resultSet = databaseHandler.getTasksByUser(1);

        while(resultSet.next()){

        }
    }


    public static void main(String[] args) {
        launch(args);
    }
}
