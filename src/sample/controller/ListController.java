package sample.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXTextField;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.image.ImageView;
import sample.database.DatabaseHandler;
import sample.model.Task;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

public class ListController {

    @FXML
    private JFXListView<Task> listTask;

    @FXML
    private JFXTextField listTaskField;

    @FXML
    private JFXTextField listDescriptionField;

    @FXML
    private JFXButton listSaveTaskButton;

    @FXML
    private ImageView listRefreshButton;

    private ObservableList<Task> tasks;
    private ObservableList<Task> refreshedTasks;

    private DatabaseHandler databaseHandler;

    @FXML
    void initialize() throws SQLException {

        tasks = FXCollections.observableArrayList();

        databaseHandler = new DatabaseHandler();
        ResultSet resultSet = databaseHandler.getTasksByUser(AddItemController.userId);
        while(resultSet.next()){
            Task task = new Task();
            task.setTaskId(resultSet.getInt("taskid"));
            task.setTask(resultSet.getString("task"));
            task.setDatecreated(resultSet.getTimestamp("datecreated"));
            task.setDescription(resultSet.getString("description"));
            System.out.println("User tasks: " + resultSet.getString("task"));

            tasks.add(task);
        }


        listTask.setItems(tasks);
        listTask.setCellFactory(CellController -> new CellController());

        listSaveTaskButton.setOnAction(actionEvent -> addNewTask());

        listRefreshButton.setOnMouseClicked(mouseEvent -> {
            try {
                refreshList();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });


    }

    public void addNewTask(){
            if(!listTaskField.getText().equals("") && !listDescriptionField.getText().equals("")) {
                Task myNewTask = new Task();
                myNewTask.setUserId(AddItemController.userId);
                myNewTask.setTask(listTaskField.getText().trim());
                myNewTask.setDescription(listDescriptionField.getText().trim());
                myNewTask.setDatecreated(java.sql.Timestamp.valueOf(LocalDateTime.now()));
                try {
                    databaseHandler.insertTask(myNewTask);
                    listTaskField.setText("");
                    listDescriptionField.setText("");

                    initialize();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
    }

    public void refreshList() throws SQLException{

        refreshedTasks = FXCollections.observableArrayList();
        databaseHandler = new DatabaseHandler();
        ResultSet resultSet = databaseHandler.getTasksByUser(AddItemController.userId);

        while(resultSet.next()){
            Task task = new Task();
            task.setTaskId(resultSet.getInt("taskid"));
            task.setTask(resultSet.getString("task"));
            task.setDatecreated(resultSet.getTimestamp("datecreated"));
            task.setDescription(resultSet.getString("description"));

            refreshedTasks.addAll(task);
        }

        listTask.setItems(refreshedTasks);
        listTask.setCellFactory(CellController -> new CellController());
    }
}

