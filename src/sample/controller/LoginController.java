package sample.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import sample.animations.Shaker;
import sample.database.DatabaseHandler;
import sample.model.User;

import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class LoginController {

    private int userId;
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private JFXButton loginSignupButton;

    @FXML
    private JFXTextField loginUsername;

    @FXML
    private JFXPasswordField loginPassword;

    @FXML
    private JFXButton loginSubmitButton;

    private DatabaseHandler dbHandler;

    @FXML
    void initialize() {
        dbHandler = new DatabaseHandler();

        loginSignupButton.setOnAction(event -> {
            goToSignUp();
        });
        loginSubmitButton.setOnAction(event -> {
            try {
                String loginText = loginUsername.getText().trim();
                String loginPwd = loginPassword.getText().trim();

                User user = new User();
                user.setUserName(loginText);
                user.setPassword(loginPwd);

                ResultSet userResult = dbHandler.getUser(user);
                int counter = 0;
                if(!loginText.equals("") && !loginPwd.equals("")){
                    while(userResult.next()){
                        counter++;
                        userId = userResult.getInt("userid");
                    }
                    if(counter == 1){
                        showAddItemScreen();
                    }else{
                        Shaker nameShaker = new Shaker(loginPassword);
                        Shaker passShaker = new Shaker(loginUsername);
                        nameShaker.shake();
                        passShaker.shake();
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } );

    }

    private void goToSignUp() {
        loginSignupButton.getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/sample/view/signup.fxml"));
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Parent root = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.showAndWait();
    }

    private void showAddItemScreen(){
        loginSignupButton.getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/sample/view/additem.fxml"));
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Parent root = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));

        AddItemController addItemController = loader.getController();
        addItemController.setUserId(userId);
        stage.showAndWait();
    }
}
