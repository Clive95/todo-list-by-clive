package sample.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.animation.FadeTransition;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;

public class AddItemController {

    public static int userId;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private AnchorPane rootAnchorPane;

    @FXML
    private ImageView addButton;

    @FXML
    private Label noTaskLabel;

    @FXML
    void initialize() {
        addButton.addEventFilter(MouseEvent.MOUSE_CLICKED, event -> {
            System.out.println("Pressed button");
            addButton.setVisible(false);
            noTaskLabel.setVisible(false);

            try {
                AnchorPane formPane = FXMLLoader.load(getClass().getResource("/sample/view/addItemForm.fxml"));

                AddItemController.userId = getUserId();

                FadeTransition transition = new FadeTransition(Duration.millis(2000),formPane);
                transition.setFromValue(0f);
                transition.setToValue(1f);
                transition.setCycleCount(1);
                transition.setAutoReverse(false);
                transition.play();
                rootAnchorPane.getChildren().setAll(formPane);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
    public int getUserId(){
        return this.userId;
    }
}
