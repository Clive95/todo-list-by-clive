package sample.controller;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import sample.database.DatabaseHandler;
import sample.model.Task;

public class AddItemFormController {

    private int userId;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private JFXTextField descriptionField;

    @FXML
    private JFXTextField textField;

    @FXML
    private JFXButton saveTaskButton;

    @FXML
    private Label infoLabel;

    @FXML
    private JFXButton todosButton;

    private DatabaseHandler databaseHandler;

    @FXML
    void initialize() {
        databaseHandler = new DatabaseHandler();
        Calendar cal = Calendar.getInstance();
        infoLabel.setVisible(false);
        saveTaskButton.setOnAction(event -> {
            Task task = new Task();

            Timestamp timestamp = new Timestamp(cal.getTimeInMillis());

            String taskText = textField.getText().trim();
            String taskDescription = descriptionField.getText().trim();

            if(!taskText.equals("") && !taskDescription.equals("")){
                task.setUserId(AddItemController.userId);
                task.setDatecreated(timestamp);
                task.setDescription(taskDescription);
                task.setTask(taskText);

                try {
                    databaseHandler.insertTask(task);
                    textField.setText("");
                    descriptionField.setText("");
                    showInfoLabel("Task successfully added!", Color.GREEN);

                } catch (SQLException e) {
                    e.printStackTrace();
                }finally {

                }
            }else{
                showInfoLabel("Task and Description should not be empty.", Color.RED);
            }
        });

        todosButton.setOnAction(actionEvent -> {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/sample/view/list.fxml"));
            try {
                loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.showAndWait();

        });
    }

    private void showInfoLabel(String text){
        infoLabel.setText(text);
        infoLabel.setTextFill(Color.WHITE);
        infoLabel.setVisible(true);

    }
    private void showInfoLabel(String text, Color color){
        infoLabel.setText(text);
        infoLabel.setTextFill(color);
        infoLabel.setVisible(true);
    }
    public int getUserId() {
        return this.userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

}